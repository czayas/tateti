#!/usr/bin/env python
#-*- coding: utf-8 -*-
'''
-------------------------------------------------------------------------------
Script: tateti
Motivo: Ejemplo de programación orientada a objetos
-------------------------------------------------------------------------------
Paradigmas de la Programación
Prof. Lic. Carlos Zayas Guggiari <carlos@zayas.org>
Licenciatura en Ciencias Informáticas
Facultad Politécnica - UNA
-------------------------------------------------------------------------------
Implementación del clásico juego de tres en raya.
-------------------------------------------------------------------------------
'''

# -----------------------------------------------------------------------------
# Importación de módulos
# -----------------------------------------------------------------------------

import os, sys, random, time

# -----------------------------------------------------------------------------
# Constantes
# -----------------------------------------------------------------------------

LF = '\n' # New Line Feed
R2 = range(2)
R3 = range(3)

# -----------------------------------------------------------------------------
# Funciones
# -----------------------------------------------------------------------------

def azar(lista):

    'Retorna un componente de la lista elegido al azar.'

    return lista and lista[random.randrange(len(lista))]

# -----------------------------------------------------------------------------

def limpiar():

    'Limpia la pantalla.'

    os.system('clear')

# -----------------------------------------------------------------------------

def pausa(tiempo=0.5):

    'Interrumpe la ejecución del programa por un tiempo determinado.'

    time.sleep(tiempo)

# -----------------------------------------------------------------------------

def out(cadena):

    'Envía una cadena a stdout y limpia el buffer para que se imprima rápido.'

    sys.stdout.write(cadena)
    sys.stdout.flush()

# -----------------------------------------------------------------------------
# Clases
# -----------------------------------------------------------------------------

class Tateti():

    'Implementación del clásico juego de tres en raya.'

    def __init__(self):

        'Constructor. Crea el tablero.'

        self.tablero = [[''] * 3 for x in R3]

    # -------------------------------------------------------------------------

    def marcar(self, fila, columna, marca):

        'Coloca la marca en la celda especificada.'

        self.tablero[fila][columna] = marca

    # -------------------------------------------------------------------------

    def libre(self, fila, columna):

        'Retorna verdadero si la celda especificada está vacía.'

        return self.tablero[fila][columna] == ''

    # -------------------------------------------------------------------------

    def libres(self):

        'Retorna una lista de coordenadas de celdas libres.'

        return [(f, c) for f in R3 for c in R3 if self.tablero[f][c] == '']

    # -------------------------------------------------------------------------

    def peligro(self, marca='X'):

        'Retorna la posición de la celda que falta marcar para ganar el juego.'

        h, v, d = self.sentidos(True)
        peligros = []

        for sentido in h, v, d:
            for fila in sentido:
                for celda in self.libres():
                    if celda in fila:
                        l = ''.join([self.tablero[f][c] for f, c in fila])
                        if l == marca * 2 and celda not in peligros:
                            peligros.append(celda)

        return azar(peligros)

    # -------------------------------------------------------------------------

    def alazar(self):

        'Retorna la posición de una celda vacía elegida al azar.'

        return azar(self.libres())

    # -------------------------------------------------------------------------

    def elegir(self):

        'Retorna la posición de una celda elegida para la computadora.'

        return self.peligro('O') or self.peligro('X') or self.alazar()

    # -------------------------------------------------------------------------

    def sentidos(self, coordenadas=False):

        'Retorna listas de coordenadas del tablero (Horiz., Vert., Diag.).'

        t = self.tablero

        if coordenadas:
            h = [[(f, c) for c in R3] for f in R3]
            v = [[(c, f) for c in R3] for f in R3]
            d = [[(abs(2 * f - c), c) for c in R3] for f in R2]
        else:
            h = [[t[f][c] for c in R3] for f in R3]
            v = [[t[c][f] for c in R3] for f in R3]
            d = [[t[abs(2 * f - c)][c] for c in R3] for f in R2]

        return h, v, d

    # -------------------------------------------------------------------------

    def linea(self):

        'Retorna verdadero si hay una línea de tres.'

        h, v, d = self.sentidos()

        return any(''.join(l) in ('XXX', 'OOO') for l in h + v + d)

    # -------------------------------------------------------------------------

    def mostrar(self, mensaje='', pos=False, tiempo=0.2):

        'Muestra el tablero y un mensaje opcional.'

        cadena = ''

        for f in R3:
            for c in R3:
                marca = '#' if pos == (f, c) else self.tablero[f][c] or ' '
                cadena += marca + '|'
            cadena = cadena[:-1] + LF
            if f < 2: cadena += '-' * 5 + LF

        limpiar()
        out(cadena + mensaje)
        if pos: pausa(tiempo)

# -----------------------------------------------------------------------------
# Bloque principal
# -----------------------------------------------------------------------------

def main():

    'Función principal.'

    tateti  = Tateti()
    mensaje = ''
    salir   = False

    while not salir:

        tateti.mostrar(mensaje)

        mensaje = ''

        if not tateti.libres():

            mensaje = "¡Empate!"
            salir = True

        else:

            marca   = 'X'
            jugada  = raw_input(LF + 'Fila, Columna (F,C)? ')

            if not jugada:

                mensaje = '¡Te rendiste!'
                salir = True

            else:

                try:

                    fila, columna = (int(item)-1 for item in jugada.split(','))

                    if tateti.libre(fila, columna):

                        tateti.marcar(fila, columna, marca)

                        if tateti.linea():
                            mensaje = '¡Ganaste!'
                            salir = True

                        else:

                            # Juega la compu:

                            marca = 'O'
                            celda = tateti.elegir()

                            if celda:

                                fila, columna = celda

                                tateti.marcar(fila, columna, marca)

                                if tateti.linea():
                                    mensaje = '¡Gané!'
                                    salir = True

                    else:

                        mensaje = 'Posición ocupada.'

                except ValueError:

                    mensaje = 'No entiendo.'

                except IndexError:

                    mensaje = 'Fuera de rango.'

    tateti.mostrar(mensaje)
    print LF + '¡Hasta luego!'

# -----------------------------------------------------------------------------
# Este archivo puede importarse como módulo gracias a la siguiente sentencia:
# -----------------------------------------------------------------------------

if __name__ == '__main__': main()
